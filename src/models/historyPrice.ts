import {Schema , model} from 'mongoose';



const historyPriceSchema  = new Schema({
    id : {type: String , unique:true},
    product_id : {type: String, unique:false },
    sellin: {type:  Number , required: true},
    price : {type:  Number , required: true},
    type : {type:  String , required: true},
    sold: {type:  Boolean , default:  false},
    createdAt: {type:  Date , default:  Date.now},
    updatedAt: {type:  Date , default:  Date.now}
});

export default model('historyPrice',historyPriceSchema)