import { Request, Response } from 'express'
import Product from '../models/product'
import historiPriceModel from '../models/historyPrice'
import historyPrice from './historiPriceControllers'
import moment from 'moment'
import config from '../config';
import _ from 'lodash';


class ProductControllers {

    //CREAR UN PRODUCTO EN LA BBDD
    async createProduct(req: Request, res: Response) {
        const { sellin, price, type, createdAt } = req.body
        const newProduct = new Product({ sellin, price, type, createdAt })
        try {
            if (!config.producTypeCreate.includes(type)) {
                res.send({
                    success: false,
                    message: "tipo de Producto no valido",
                    producTypes: config.producTypeCreate
                })
                return
            }
            if (type == "Mega cobertura" && price != 180) {
                res.send({
                    success: false,
                    message: "monto invalido para tipo de producto => el monto debe ser 180",
                    price: price
                })
                return
            }
            if (price >= 100 || price <= 0) {
                res.send({
                    success: false,
                    message: "monto invalido este debe estar entre 1 y 99",
                    price: price
                })
                return
            }
            await newProduct.save(async (err) => {
                if (err) {
                    console.log("ERROR AL CREAR EL PRODUCTO", err);
                    res.send({
                        success: false,
                        mesagge: 'ERROR AL CREAR EL PRODUCTO',
                        data: err,
                        body: req.body
                    })
                } else {
                    // SE HA CREADO EL PRODUCTO CORRECTAMENTE Y SE GUARDA SU HISTORICO 
                    historyPrice.createHistoryPrice(sellin, price, type, newProduct._id);
                    res.send({
                        success: true,
                        mesagge: 'producto creado',
                        data: newProduct,
                        body: req.body
                    })
                }
            });
        } catch (error) {
            console.log(error.stack)
            res.send({
                success: false,
                mesagge: 'error catch create product',
                data: error.stack
            })
        }
    }

    //OBTENER TODOS LOS PRODUCTOS EXISTENTE EN LA BBDD
    async getAllProducts(req: Request, res: Response) {
        try {
            res.send(await Product.find({ sold: false }))
            return (await Product.find({ sold: false }))
        } catch (error) {
            res.send({
                success: false,
                mesagge: `error get getAllProducts `,
                error
            })
        }
    }

    //OBTENER TODOS LOS PRODCUTOS VENDIDOS 
    async getAllSoldProducts(req: Request, res: Response) {
        try {
            res.send(await Product.find({ sold: true }))
        } catch (error) {
            res.send({
                success: false,
                mesagge: `error get AllSoldProducts `,
                error
            })
        }
    }

    //VENDER UN PRODUCTO
    async sellProduct(req: Request, res: Response) {
        const { _id } = req.body
        try {
            const soldProduct = await Product.findOneAndUpdate({ _id }, { sold: true });

            // await historiPriceModel.updateMany({ product_id: _id }, { sold: true });
            historyPrice.createHistoryPrice(soldProduct.sellin, soldProduct.price, soldProduct.type, soldProduct._id, true);
            // SE RETORNA LA LISTA DEL PRODUCTO + SU HISTORICO DE PRECIOS ACUTALIZADO
            res.send({
                data: [
                    await Product.find({ _id }),
                    await historiPriceModel.find({ product_id: _id })
                ]
            })

        } catch (error) {
            res.send({
                success: false,
                mesagge: `error sellProduct => no existe producto con ID ${_id} o ya fue vendido `,
                error
            })
        }
    }



    async updatePrices(req: Request, res: Response) {

        let newProduct: any;
        let newPrice: number;
        let newSellin: number;
        let suma: number;
        let newPriceArray = [];
        try {
            const allProduct = await Product.find({ sold: false });
            const oldProduct = allProduct.filter((prod: any) => {
                return (moment().diff(prod.updatedAt, 'days') > 0)
            });
            //SI EXISTE PRODUCTOS PARA ACUTALIZAR SUS PRECIOS 

            if (oldProduct.length > 0) {
                for (let product of oldProduct) {

                    let { type, _id } = product
                    newSellin = product.sellin - 1

                    if (product.price < 100 || product.price > 0) {

                        if (config.producTypeSpecial.includes(product.type)) {

                            //PRODUCTO FULL COBERTURA Y FULL COBERTURA SUPER DUPER CON SELLIN SOBRE 0
                            if (product.type == "Full cobertura" || "Full cobertura Super duper") {


                                if (product.sellin <= 1) {
                                    newPrice = 0
                                    newSellin = 0
                                } else {

                                    if (product.sellin > 10) {
                                        if (product.type == "Full cobertura Super duper") {

                                            product.price - 1 <= 0 ? newPrice = 0 : newPrice = product.price - 1
                                        } else {
                                            product.price + 1 >= 100 ? newPrice = 100 : newPrice = product.price + 1
                                        }

                                    } else {

                                        if (product.sellin <= 10 && product.sellin > 5) {
                                            suma = 2
                                        }

                                        if (product.sellin <= 5 && product.sellin > 0) {
                                            suma = 3
                                        }
                                        product.price + suma > 100 ? newPrice = 100 : newPrice = product.price + suma

                                    }
                                }
                            }

                        } //CASOS DE DEPRECACION NORMALES 
                        else {
                            if (product.sellin <= 0) {
                                suma = 2
                            } else {
                                suma = 1
                            }

                            // REGLAS DE NEGOCIO PARA SUPER AVANCE ... SU DEPRECACION ES EL DOBLE DE RAPIDO 
                            product.type == "Super avance" ? suma * 2 : suma * 1
                            product.price - suma <= 0 ? newPrice = 0 : newPrice = product.price - suma
                        }

                        console.log(product)
                        newProduct = await Product.findOneAndUpdate(
                            { _id: product._id },
                            {
                                price: newPrice,
                                sellin: newSellin,
                                updatedAt: new Date()
                            });

                        historyPrice.createHistoryPrice(newProduct.sellin, newProduct.price, type, _id);
                        newProduct = await Product.find({ _id: product._id })


                    }                    
                    newPriceArray.push(newProduct[0])
                }

            } else {
                //no existen productos para deprecar
                res.send({
                    success: false,
                    message: "No se encontraron productos para acutalizar ",
                    data: oldProduct
                })
                return
            }

            res.send({
                success: true,
                message: `actualizacion de precios y sellin exitoso => se actualirazon ${newPriceArray.length}`,
                data: newPriceArray
            })


        } catch (error) {
            res.send({
                success: false,
                message: "error catch evaluar productos ",
                data: error
            })
        }



    }

    async evaluateProductsHistory(req: Request, res: Response) {
        const days = parseInt(req.params.url);
        let historyProductArray = [];
        let productDaysArray = [];
        let finalProductArray = [];
        let resProduct:any;


        const allProduct = await Product.find({})

        const uniqueID = _.map(allProduct, '_id')

        //SE GENERA UNA MATRIZ VACIA PARA MOSTRAR LOS DATOS ORDENADOS
        for (let array = 0; array < days; array++) {
            productDaysArray = []

            //SE RECORRE LOS ID UNICOS PARA OBTENER EL HISTORICOS DE PRECIO DE CADA PRODUCTO 
            for (let id of uniqueID) {
                historyProductArray = await historiPriceModel.find({ product_id: id })

                //SE VALIDA LA EXISTENCIA DE DATOS Y QUE ESTOS CUMPLAN CON EL LARGO SOLICITADO 
                if (historyProductArray.length) {
                    historyProductArray = historyProductArray.slice(0, days);
                    if (historyProductArray[array]) {
                        resProduct = {
                            idProducto :historyProductArray[array].product_id,
                            type: historyProductArray[array].type,
                            sold: historyProductArray[array].sold,
                            sellin: historyProductArray[array].sellin,
                            price: historyProductArray[array].price,
                        }
                        productDaysArray.push(resProduct)
                    }
                }

            }
            finalProductArray.push(productDaysArray)
        }
        res.send({
            success: true,
            messagge:  `comportamiento de los productos durante ${days} dias`,
            data: finalProductArray
        })
    }

    //OBTENER UN PRODUCTO ESPECIFICO DE LA BBDD
    getProduct(req: Request, res: Response) {
        console.log(req.body)
        res.json("buscando producto")
    }

    //BORRAR UN PRODUCTO EN LA BBDD
    deleteProduct(req: Request, res: Response) {

    }

    //ACTUALIZAR UN PRODUCTO EN LA BBDD
    updateProduct(req: Request, res: Response) {

    }

}

const productControllers = new ProductControllers()

export default productControllers;