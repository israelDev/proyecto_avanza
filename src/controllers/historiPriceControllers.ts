
import historyPrice from '../models/historyPrice'

class HistoryPriceControllers {

        //CREAR UN HISTORICO DE PRESIOS Y SELLIN EN LA BBDD
        async createHistoryPrice(sellin:Number, price:Number, type:String ,product_id:String ,sold = false ) {
 
            const newhistoryPrice = new historyPrice({ sellin, price, type ,product_id , sold})
    
            await newhistoryPrice.save(async (err) => {
                if (err) {
                    console.log("ERROR AL CREAR EL PRODUCTO", err);
                    return{success: false,
                        mesagge: 'ERROR AL CREAR EL PRODUCTO',
                        data: err,
                        body: { sellin, price, type ,product_id}
                    }
                        
                } else {
                    return{
                        success: true,
                        mesagge: 'historico de precio creado',
                        data: newhistoryPrice,
                        body: { sellin, price, type ,product_id}
                    }
                }
            });
        }

}

const historyPriceControllers = new HistoryPriceControllers()

export default historyPriceControllers;