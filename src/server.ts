import express from 'express';
import morgan from 'morgan';
import helmet from 'helmet';
import mongoose from 'mongoose';
import compression from 'compression';
import cors from 'cors';
import indexRoutes from './routes/index';
import productRoutes from './routes/productsRoutes';
import Config from './config';

class Server {
    public app: express.Application;
    constructor() {
        this.app = express();
        this.config();
        this.routes();
    }
    config() {
        const MONGO_URI = 'mongodb://localhost/restapi'
        mongoose.set('useFindAndModify'|| process.env.MONGO_URI, false);
        mongoose.connect(MONGO_URI, {
            useNewUrlParser: true,
            useCreateIndex: true
        }).then(db => console.log("db is connected"))

        this.app.set('port', Config.PORT|| 3000);
        this.app.use(morgan('dev'))
        this.app.use(express.json()); // manejo de json en body parse
        this.app.use(express.urlencoded({extended: false})); // validacion de formulario 
        this.app.use(helmet());
        this.app.use(compression()); // compresion de reponse 
        this.app.use(cors()); // validar conexion con servidores 
    }
    routes() {
        this.app.use(indexRoutes);
        this.app.use('/product',productRoutes);
    }
    start() {
        this.app.listen(this.app.get('port'), () => {
            console.log('listen on port', this.app.get('port'));
        });
    }

}

const server = new Server();
server.start();