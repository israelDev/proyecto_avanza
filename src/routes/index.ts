import {Request , Response , Router} from 'express'

class  IndexRoutes {
    router : Router;
    constructor(){
        this.router = Router();
        this.routes();
    }
    routes(){
        this.router.get('/index', (req, res) => res.send( `/product/create => crea un nuevo producto en la BBD \n
        /product/getAll => muestra una lista con todos los porductos aun sin vender \n
        /product/getAllSold => muestra una lista de todos los productos vendidos \n
        /product/updatePrices => actualiza los precios de los productos no vendidos aplicando reglas de negocio \n
        /product/evaluateProducts/ => muestra un listado del comportamiento de los productos en un rango determinado de dias \n
        /product/sellProduct => vende un producto especifico`))
    }
    
} 

const indexRoutes = new IndexRoutes();
indexRoutes.routes();

export default indexRoutes.router;

