import { Router } from 'express'
import controllersPrduct from '../controllers/productControllers'


class ProductsRoutes {
    router: Router;
    constructor() {
        this.router = Router();
        this.routes();
    }
    routes() {
        this.router.post('/create', controllersPrduct.createProduct);
        this.router.get('/getAll', controllersPrduct.getAllProducts);
        this.router.get('/getAllSold', controllersPrduct.getAllSoldProducts);
        this.router.get('/updatePrices', controllersPrduct.updatePrices);
        this.router.get('/evaluateProducts/:url', controllersPrduct.evaluateProductsHistory);
        this.router.post('/sellProduct', controllersPrduct.sellProduct);
        this.router.post('/get/:url', controllersPrduct.getProduct);
        this.router.put('/update/:url', controllersPrduct.updateProduct);
        this.router.put('/delete/:url', controllersPrduct.deleteProduct);
    }

}

const productRoutes = new ProductsRoutes();
productRoutes.routes();

export default productRoutes.router;

